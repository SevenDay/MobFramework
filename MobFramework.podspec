Pod::Spec.new do |s|
  s.name         = "MobFramework"
  s.version      = "0.0.1"
  s.summary      = "MobFramework for 7days"
  s.description  = <<-DESC  "MobFramework for 7days App"
                   DESC

  s.homepage     = "https://gitlab.com/SevenDay/MobFramework"
  s.license      = ""
  s.author             = { "zhang.wenhai" => "zhang_mr1989@163.com" }
  s.platform     = :ios, "8.0"

  s.source       = { :git => "git@gitlab.com:SevenDay/MobFramework.git" }

  # s.source_files  = "TencentIMSDK", "TencentIMSDK/**/*.{h,m}"
  #s.exclude_files = "TencentIMSDK/Info.plist"
  s.vendored_frameworks = "Framework/*.framework"

  # s.public_header_files = "Classes/**/*.h"
 

  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  A list of resources included with the Pod. These are copied into the
  #  target bundle with a build phase script. Anything else will be cleaned.
  #  You can preserve files from being cleaned, please don't preserve
  #  non-essential files like tests, examples and documentation.
  #

  # s.resource  = "icon.png"
  # s.resources = "Resources/*.png"

  # s.preserve_paths = "FilesToSave", "MoreFilesToSave"


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  # s.framework  = "SomeFramework"
  s.frameworks = "MessageUI", "JavaScriptCore","SystemConfiguration","CoreTelephony"

  # s.library   = "iconv"
  s.libraries = "z","icucore","stdc++"

  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # s.dependency "JSONKit", "~> 1.4"

end
